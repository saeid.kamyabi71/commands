# run container

docker run --name ocserv --privileged -p 8390:443 -p 8390:443/udp -e CA_CN="saeed" -e CA_ORG="saeed" -e CA_DAYS=3650 -e SRV_CN=saeed.com -e SRV_ORG="saeed" -e SRV_DAYS=365 -e NO_TEST_USER=1 -v ~/ocserv:/etc/ocserv --restart=always -d tommylau/ocserv

- after creating container, we should remove auto group select of ocserv.conf (remove last lines of config file) 

# add new user

docker exec -ti ocserv ocpasswd -c /etc/ocserv/ocpasswd -g "Route,All" saeed


reference: https://github.com/TommyLau/docker-ocserv


