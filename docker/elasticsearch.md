docker pull elasticsearch:5.4.0

docker run --name elasticsearch -v ~/docker/elasticsearch:/usr/share/elasticsearch/data -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -d elasticsearch:5.4.0


reference: https://www.elastic.co/guide/en/elasticsearch/reference/5.5/docker.html